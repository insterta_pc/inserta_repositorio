﻿using System;
using System.IO;
using UnityEngine;

public class ManejadorEstadoPartida: MonoBehaviour
{
    private const string RUTA_ESTADO_PARTIDA = "EstadoPartida.json";

    private static InformacionPartida informacionPartida = new InformacionPartida();

    public void RestablecerPartida()
    {
        StreamReader reader = new StreamReader(RUTA_ESTADO_PARTIDA);
        string informacionPartidaJson = reader.ReadToEnd();
        informacionPartida = JsonUtility.FromJson<InformacionPartida>(informacionPartidaJson);
        reader.Close();
    }

    public void GuardarPartida()
    {
        StreamWriter writer = new StreamWriter(RUTA_ESTADO_PARTIDA, false);
        writer.Write(JsonUtility.ToJson(informacionPartida));
        writer.Close();
    }

    public string CombinacionActual
    {
        get
        {
            return informacionPartida.CombinacionActual;
        }
        set
        {
            informacionPartida.CombinacionActual = value;
            informacionPartida.PalabrasIntroducidasEnCombinacionActual.Clear();
            informacionPartida.CombinacionesUsadas.Add(value);
        }
    }

    public bool EstaLaPalabraUtilizada(string palabra)
    {
        return informacionPartida.PalabrasIntroducidasEnCombinacionActual.Contains(palabra);
    }

    public void IncrementarPuntos(int incremento)
    {
        informacionPartida.Puntos += incremento;
    }

    public int GetPuntos()
    {
        return informacionPartida.Puntos;
    }

    public void AddPalabraUsadaEnCombinacionActual(string palabra)
    {
        informacionPartida.PalabrasIntroducidasEnCombinacionActual.Add(palabra);
    }

    public bool HaSidoCombinacionUsadaYa(string combinacion)
    {
        return informacionPartida.CombinacionesUsadas.Contains(combinacion);
    }

    public int NumeroPalabrasUsadaEnCombinacionActual
    {
        get
        {
            return informacionPartida.PalabrasIntroducidasEnCombinacionActual.Count;
        }
    }
}
