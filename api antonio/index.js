'use strict'

// Librerías
const mongoose   = require('mongoose')
const app        = require('./app')
const config     = require('./config')

// Conexión de mongoose
mongoose.Promise = global.Promise
mongoose.connect(config.db, (err, res) => {

    if (err) {
        return console.log(`Error al conectar a la base de datos: ${err}`)
    }

    console.log('Conexión a la base de datos '+ config.db +' establecida...');
  
    app.listen(config.port, () => {
        console.log(`Api ejecutada en en http://localhost:${config.port}`)
    })

})
