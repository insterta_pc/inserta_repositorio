'use strict'

const mongoose = require('mongoose')
const Schema   = mongoose.Schema

const Points_schema = new Schema({

    // first_update : { type: Date, default: Date.now() },
    // last_update  : { type: Date, default: Date.now() },
    // // email        : { type: String, lowercase: true },
    // username     : { type: String, default:""},
    // puntuacion   : { type: Number, default:0}

    username     : String,
    puntuacion   : Number

})

Points_schema.pre("save", function(next) {
    if(!this.trial){
        //do your job here
        next();
    }
});

// Points_schema.pre('save', function(next) {
//     console.log("Saving !");
//     let user = this;
//     next()
// });

module.exports = mongoose.model('Points', Points_schema)
