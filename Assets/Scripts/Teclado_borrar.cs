﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Teclado_borrar : MonoBehaviour
{
    public Text texto;

    // Función que elimina el último carácter en un texto seleccionado
    public void Borrar()
    {
        if (texto.text.Length > 0)
        {
            texto.text = texto.text.Remove(texto.text.Length - 1);
        }
    }
}
