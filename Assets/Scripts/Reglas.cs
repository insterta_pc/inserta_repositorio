﻿using UnityEngine;
using UnityEngine.SceneManagement;

// Gestor del principio de la escena
public class Reglas : MonoBehaviour
{
    public AudioSource click;

    public void LoadInicio()
    {
        click.Play();
        SceneManager.LoadScene("Inicio");
    }

    public void LoadNextLevel()
    {
        click.Play();
        SceneManager.LoadScene("Principal");
    }
}
