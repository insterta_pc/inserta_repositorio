﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

[Serializable]
public class InformacionPartida
{
    // Esta clase guarda un registro de cómo está progresando la partida
    public InformacionPartida()
    {
        Puntos = 0;
        PalabrasIntroducidasEnCombinacionActual = new List<string>();
        CombinacionesUsadas = new List<string>();
    }

    public string CombinacionActual;
    public List<string> PalabrasIntroducidasEnCombinacionActual;
    public int Puntos;
    public List<string> CombinacionesUsadas;
}
