﻿using UnityEngine;
using System.Data;
using Mono.Data.SqliteClient;
using System.Collections.Generic;
using System.IO;
using System;

public class BBDD : MonoBehaviour {
    public Textos textos;
    IDbConnection conexion;

    public void Awake()
    {
        string databaseName = "Combinaciones.db";
        string filepath = Application.streamingAssetsPath + "/" + databaseName;

        if (!File.Exists(filepath))
        {
            WWW loadDB = new WWW("jar:file://" + Application.dataPath + "!/assets/" + databaseName); 

            while (!loadDB.isDone) { }  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, 
            //place a timer and error check

            File.WriteAllBytes(filepath, loadDB.bytes);
        }

        conexion = new SqliteConnection("URI=file:" + filepath);
    }

    public List<string> ExtraerCombinaciones()
    {
        conexion.Open();

        IDbCommand cmd = conexion.CreateCommand();
        cmd.CommandText = "SELECT combination FROM words";
        IDataReader datos = cmd.ExecuteReader();

        List<string> result = new List<string>();
        while (datos.Read())
        {
            result.Add(datos.GetString(0));
        }
        conexion.Close();

        return result;
    }

    public int ExtraerPalabrasParaCombinacion(string combinacion)
    {
        conexion.Open();

        IDbCommand cmd = conexion.CreateCommand();
        cmd.CommandText = "SELECT COUNT(*) FROM words WHERE combination = '" + combinacion + "'";
        IDataReader datos = cmd.ExecuteReader();

        int cuenta = 0;
        while (datos.Read())
        {
            cuenta = datos.GetInt32(0);
            Debug.Log("Cuenta: " + cuenta);
        }
        conexion.Close();

        return cuenta;
    }

    public int ExtraerPuntos (string combinacion, string palabra)
    {
        conexion.Open();

        IDbCommand cmd = conexion.CreateCommand();
        cmd.CommandText = "SELECT points FROM words WHERE combination = '" + combinacion + "' AND word = '" + palabra + "'";
        IDataReader datos = cmd.ExecuteReader();

        int puntos = 0;
        while (datos.Read())
        {
            puntos = datos.GetInt32(0);
            Debug.Log("Puntos: " + puntos);
        }
        conexion.Close();

        return puntos;
    }
}
